﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class ExtensionsTest {

    [Test]
    public void Rect_FitPosition_ContainsZero() {
        var pos = Vector3.zero;
        var rect = new Rect(0, 0, 5, 20);
        
        var resultPos = rect.FitPosition(pos); 
        Assert.AreEqual(resultPos, pos);
    }

    [Test]
    public void Rect_FitPosition_FitsTooHigh() {
        var pos = new Vector3(0, 100, 0);
        var expectedPos = new Vector3(0, 10, 0);
        var rect = new Rect(-2.5f, -10f, 5, 20);
        
        var resultPos = rect.FitPosition(pos); 
        Assert.AreEqual(resultPos, expectedPos);
    }


}
