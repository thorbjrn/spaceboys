using UnityEngine;

public class ParticlesManager : MonoBehaviour
{
    internal static ParticlesManager PM;
    [SerializeField] private ParticleSystem explosionSystem;

    void Awake()
    {
        PM = this;
    }

    public void AddExplosionAt(Vector3 pos)
    {
        // Debug.Log("AddExplosionAt - pos: "+ pos);

        pos.z = -10f;
        var emitParams = new ParticleSystem.EmitParams();
        emitParams.position = pos;
        explosionSystem.Emit(emitParams, 1);
    }
}
