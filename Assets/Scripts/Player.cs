﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Player : MonoBehaviour {

	[SerializeField] private MeshRenderer shipRenderer;
	[SerializeField] private float speed;

	private int playerId;
	private Faction faction;
    private int joystickId;
	public int JoystickId { get { return joystickId; } }
    private Action<int, int> onAction;

	private Vector2 movement;

	private Material shipMat;

	private float resourceAmount;
	private float resourceIncome;

	public float ResourceAmount { get { return resourceAmount; } set{ resourceAmount = value;} }

	void Awake()
	{
		shipMat = shipRenderer.material;
	}

	void Update()
	{
		if (joystickId == -1)
		{
			movement.x = Input.GetAxis("Horizontal");
			movement.y = Input.GetAxis("Vertical");
		}else
		{
			movement.x = Input.GetAxis("L_XAxis_" + joystickId);
			movement.y = Input.GetAxis("L_YAxis_" + joystickId);
		}

		resourceAmount = math.min(resourceAmount + resourceIncome * Time.deltaTime, 10000f); //TODO
		MenuManager.MM.UpdateResourceDial(playerId, resourceAmount);
		
		movement *= speed;
		
		transform.Translate(movement * Time.deltaTime);
		transform.position = faction.spawnArea.FitPosition(transform.position);

		if (joystickId == -1)
		{
			if (Input.GetKey(KeyCode.Z)) onAction(playerId, 0);
			else if (Input.GetKey(KeyCode.X)) onAction(playerId, 1);
		}else
		{
			if (Input.GetButton("A_" + joystickId)) onAction(playerId, 0);
			else if (Input.GetButton("X_" + joystickId)) onAction(playerId, 1);
		}
		
	}

    internal void Initialize(int playerId, Faction faction, int joystickId, Action<int, int> onPlayerAction)
    {
		this.playerId = playerId;
		this.faction = faction;
		this.joystickId = joystickId;
		shipMat.color = faction.color;
		if (playerId == 1) shipRenderer.transform.localRotation = Quaternion.Euler(0, 0,  180f);
		
		resourceAmount = GameplayManager.GM.StartResourceAmount;
		resourceIncome = GameplayManager.GM.StartResourceIncome;

		MenuManager.MM.UpdateResourceDial(playerId, resourceAmount);

        onAction += onPlayerAction;
    }


}
