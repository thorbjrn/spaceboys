﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSelectMenu : MonoBehaviour {

	[SerializeField] private PlayerSelectPanel[] panels;

	List<int> playerJoysticks = new List<int>();


	void Update()
	{
		for (int id = 1; id <= 4; id++)
		{
			if (Input.GetButtonDown("A_" + id))
			{
				JoinWithJoystick(id);
			}
		}
	}

    private void JoinWithJoystick(int joystickId)
    {
		if (playerJoysticks.Contains(joystickId)) return;
		if (playerJoysticks.Count == 2) return;

		int playerIdx = playerJoysticks.Count;
		panels[playerIdx].PlayerJoined(playerIdx);
        playerJoysticks.Add(joystickId);;

		if (playerJoysticks.Count == 2)
		{
			MenuManager.MM.PlayersJoined(playerJoysticks);
		}
    }
}
