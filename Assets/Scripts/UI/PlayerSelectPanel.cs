﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelectPanel : MonoBehaviour 
{
	[SerializeField] private Text infoText;


    internal void PlayerJoined(int idx)
    {
		infoText.text = "Player " + (idx + 1) + " joined";
    }
}
