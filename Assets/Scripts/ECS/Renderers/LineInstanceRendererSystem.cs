using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using Unity.Entities;
using Unity.Burst;

public class LineInstanceRendererSystem : JobComponentSystem
{
	public struct ParticleLines
	{
        public readonly int Length;
		public ComponentDataArray<ParticleLine> data;
	}

	[Inject]
	private ParticleLines lines;

	private Material lineMaterial;
	private Mesh lineMesh;

	private ComputeBuffer argsBuffer;
	private ComputeBuffer objectToWorldBuffer;
	private ComputeBuffer colorBuffer;

	private const int Capacity = 50000;

	[NativeFixedLength(5)]
	private NativeArray<uint> indirectArgs;
	[NativeFixedLength(Capacity)]
	private NativeArray<Matrix4x4> transformationMatrices;
	private NativeArray<float4> colors;

	private JobHandle previousFrameHandle;
	public int prevLength = 0;

	protected override void OnDestroyManager()
	{
		base.OnDestroyManager();

		if (argsBuffer != null) argsBuffer.Dispose();
		if (objectToWorldBuffer != null) objectToWorldBuffer.Dispose();

		previousFrameHandle.Complete();
		if (transformationMatrices.IsCreated) transformationMatrices.Dispose();
		if (colors.IsCreated) colors.Dispose();
		if (indirectArgs.IsCreated) indirectArgs.Dispose();
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		// Allocate the native array
		if (!indirectArgs.IsCreated) indirectArgs = new NativeArray<uint>(5, Allocator.Persistent);
		if (!transformationMatrices.IsCreated) transformationMatrices = new NativeArray<Matrix4x4>(Capacity, Allocator.Persistent);
		if (!colors.IsCreated) colors = new NativeArray<float4>(Capacity, Allocator.Persistent);

		if ((lineMaterial == null || lineMesh == null) && Application.isPlaying)
		{
			var lineObject = GameObject.Find("LineTemplate");
			if (lineObject != null)
			{
				argsBuffer = new ComputeBuffer(1, indirectArgs.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
				objectToWorldBuffer = new ComputeBuffer(Capacity, 64);
				colorBuffer = new ComputeBuffer(Capacity, 64);

				lineMesh = lineObject.GetComponent<MeshFilter>().sharedMesh;
				lineMaterial = new Material(lineObject.GetComponent<Renderer>().sharedMaterial);

				
				lineMaterial.SetFloat("textureCoord", 1);
			}
			else Debug.LogError("Line object not found");
		}

		if (lineMaterial == null || lineMesh == null) return inputDeps;

		lineMaterial.SetBuffer("objectToWorldBuffer", objectToWorldBuffer);
		lineMaterial.SetBuffer("colorBuffer", colorBuffer);

		previousFrameHandle.Complete();

		objectToWorldBuffer.SetData(transformationMatrices);
		colorBuffer.SetData(colors);
		// Setup the args buffer
		indirectArgs[0] = lineMesh.GetIndexCount(0);
		indirectArgs[1] = (uint)prevLength;
		argsBuffer.SetData(indirectArgs);

		Graphics.DrawMeshInstancedIndirect(lineMesh, 0, lineMaterial, new Bounds(Vector3.zero, 10000000 * Vector3.one), argsBuffer);

		prevLength = lines.Length;
		var calculateMatricesHandle = new CalculateLinesTransformationMatrix
		{
			lines = lines.data,
			transformationMatrices = transformationMatrices,
		}.Schedule(lines.Length, 8, inputDeps);

		var updateLifeTimeHandle = new UpdateLifetime
		{
			Lines = lines.data,
			Colors = colors,
			DeltaTime = Time.deltaTime
		}.Schedule(lines.Length, 8, calculateMatricesHandle);


		previousFrameHandle = updateLifeTimeHandle;
		return previousFrameHandle;
	}

	[BurstCompile]
	private struct CalculateLinesTransformationMatrix : IJobParallelFor
	{
		[ReadOnly]
		public ComponentDataArray<ParticleLine> lines;
		[NativeFixedLength(Capacity)]
		public NativeArray<Matrix4x4> transformationMatrices;

		public void Execute(int index)
		{
			var line = lines[index];

			// float3 f = math.normalize(new float3(1, 0, 0));
			// float3 r = math.cross(f, new float3(0, 1, 0));
			// float3 u = math.cross(f, r);
			// float3 p = line.StartPosition.Value;

			// // Just add some scale to the projectiles, later remove this

			// var transform = new Matrix4x4(	new Vector4(r.x, r.y, r.z, 0),
			// 								new Vector4(u.x, u.y, u.z, 0),
			// 								new Vector4(f.x, f.y, f.z, 0),
			// 								new Vector4(p.x, p.y, p.z, 1f));

            var diff = line.EndPosition.Value - line.StartPosition.Value;
            var dir = math.normalize(diff);
            float dist = math.length(diff);

            var pos = line.StartPosition.Value + diff / 2f;
            var rot = quaternion.euler(0, 0,  math.atan2(dir.y, dir.x));
            var scale = new float3(dist, .2f, 1f);

            var transform = Matrix4x4.identity;
            transform.SetTRS(pos, rot, scale); 

			transformationMatrices[index] = transform;
		}
	}


	// [BurstCompile]
	private struct UpdateLifetime : IJobParallelFor
	{
		public ComponentDataArray<ParticleLine> Lines;
		[NativeFixedLength(Capacity)]
		public NativeArray<float4> Colors;
		public float DeltaTime;


		public void Execute(int index)
		{
			var line = Lines[index];
			line.LifeTime = math.max(line.LifeTime - DeltaTime, 0);
			
			if (line.LifeTime <= 0)
			{
				//TODO
			}

			float alpha = line.LifeTime / 0.25f; //TODO
			if (line.PlayerId == 0) Colors[index] = new float4(1, 0, 0, alpha);
			if (line.PlayerId == 1) Colors[index] = new float4(0, 0, 1, alpha);
			// Debug.Log("alpha: "+ alpha + ", line.LifeTime: "+ line.LifeTime + ", index: "+ index);
			Lines[index] = line;
		}
	}
}
