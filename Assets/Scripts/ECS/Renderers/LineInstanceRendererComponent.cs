using System;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// Render Mesh with Material (must be instanced material) by object to world matrix.
/// Specified by TransformMatrix associated with Entity.
/// </summary>
[Serializable]
public struct LineInstanceRenderer : ISharedComponentData
{
    public Mesh                 mesh;
    public Position             startPosition;
    public Position             endPosition;
    public Material             material;
}

public class LineInstanceRendererComponent : SharedComponentDataWrapper<LineInstanceRenderer> { }

