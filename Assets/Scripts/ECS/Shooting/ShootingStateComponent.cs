using System;
using Unity.Entities;

public class ShootingStateComponent : ComponentDataWrapper<ShootingState>{}

[Serializable]
public struct ShootingState : IComponentData
{
	public float Cooldown;
	public Entity TargetEntity;

}
