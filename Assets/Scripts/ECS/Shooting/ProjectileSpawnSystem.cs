﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


public class ProjectileSpawnBarrier : BarrierSystem {}

public class ProjectileSpawnSystem : ComponentSystem {

    public struct SpawnData
    {
        public readonly int Length;
        public EntityArray SpawnedEntities;
        [ReadOnly] public ComponentDataArray<ProjectileSpawnData> Spawn;
    }

    [Inject] private SpawnData spawnData;

    protected override void OnUpdate()
    {
        var entityManager = PostUpdateCommands;

        for (int i = 0; i < spawnData.Length; ++i)
        {
            var spawn = this.spawnData.Spawn[i];
            var projectileEntity = this.spawnData.SpawnedEntities[i];

            //Change spawn entity into shot entity, by removing and adding components
            entityManager.RemoveComponent<ProjectileSpawnData>(projectileEntity);

            entityManager.AddComponent(projectileEntity, spawn.Position);
            entityManager.AddComponent(projectileEntity, spawn.Rotation);
            entityManager.AddComponent(projectileEntity, default(TransformMatrix));
            entityManager.AddComponent(projectileEntity, new Projectile { PlayerId = spawn.PlayerId});
            // entityManager.AddComponent(projectileEntity, new MoveSpeed {speed = 10});
            entityManager.AddSharedComponent(projectileEntity, GameplayManager.GM.ProjectileRenderer);
            
        }
    }
}

