﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(FlockingSystem))]
public class ShootingSystem : JobComponentSystem {

	public struct UnitData
	{
		public readonly int Length;
		[ReadOnly] public ComponentDataArray<Position> Positions;
		[ReadOnly] public ComponentDataArray<PlayerUnit> PlayerUnits;
		public ComponentDataArray<HealthState> Healths;
		[ReadOnly] public EntityArray Entities;
	}


	public struct ShooterData
	{
		[NativeDisableParallelForRestriction]
		public ComponentDataFromEntity<Velocity> VelocityEntities;
		[ReadOnly]
		public ComponentDataFromEntity<Shooting> ShooterEntities;
		[NativeDisableParallelForRestriction]
		public ComponentDataFromEntity<ShootingState> ShootingStateEntities;
		[NativeDisableParallelForRestriction]
		public ComponentDataFromEntity<Rotation> RotationEntities;
		[NativeDisableParallelForRestriction]
		public ComponentDataFromEntity<Target> TargetEntities;
		[NativeDisableParallelForRestriction]
		public ComponentDataFromEntity<Constructable> ConstrucableEntities;
	}

	[Inject] private UnitData unitData;
	private ShooterData shooterData;
	[Inject] private ComponentDataFromEntity<Velocity> velocityEntities;
	[Inject] private ComponentDataFromEntity<Shooting> shooterEntities;
	[Inject] private ComponentDataFromEntity<ShootingState> shootingStateEntities;
	[Inject] private ComponentDataFromEntity<Rotation> rotationEntities;
	[Inject] private ComponentDataFromEntity<Target> targetEntities;
	[Inject] private ComponentDataFromEntity<Constructable> construcableEntities;
	[Inject] private ProjectileSpawnBarrier shotSpawnBarrier;
	[Inject] private CellingSystem cellingSystem;


	struct PrevCells
	{
		public NativeArray<int> NearestEnemyIndices;
		public NativeArray<float> NearestEnemyDistance;
    }
	private PrevCells prevCells = new PrevCells();

	private bool isFirst = true;


	protected override JobHandle OnUpdate(JobHandle inputHandle)
	{
		int unitCount = unitData.Length;

		shooterData.VelocityEntities = velocityEntities;
		shooterData.ShooterEntities = shooterEntities;
		shooterData.ShootingStateEntities = shootingStateEntities;
		shooterData.RotationEntities = rotationEntities;
		shooterData.TargetEntities = targetEntities;
		shooterData.ConstrucableEntities = construcableEntities;

		var nearestEnemyIndices 		= new NativeArray<int>(unitCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		var nearestEnemyDistance 		= new NativeArray<float>(unitCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);

		var nextCells = new PrevCells
		{
			NearestEnemyIndices 	= nearestEnemyIndices,	
			NearestEnemyDistance 	= nearestEnemyDistance,
		};

		if (!isFirst) DisposePrevious();
		isFirst = false;
		prevCells = nextCells;

#region Job Sceheduling

		var findNearestHandle = new FindNearestEnemy
		{
			UnitData					= unitData,
			ShooterData					= shooterData,
			CellMap						= cellingSystem.CellMap,
			NearestEnemyIndices       	= nearestEnemyIndices,
			NearestEnemyDistance     	= nearestEnemyDistance,
		}.Schedule(unitCount, 64, inputHandle);
		
		var targetEnemyHandle = new TargetEnemy
		{
			UnitData					= unitData,
			ShooterData					= shooterData,
			NearestEnemyDistance 		= nearestEnemyDistance,
			NearestEnemyIndices 		= nearestEnemyIndices,
			RotationSpeed				= 180,
			DeltaTime					= Time.deltaTime,
			Time						= Time.time,
		}.Schedule(unitCount, 64, findNearestHandle);

		var shootHandle = new ShootAtEnemy
		{
			UnitData							= unitData,
			ShooterData							= shooterData,
			// ShooterToUnitMap					= shooterToUnitMap,
			NearestEnemyIndices       			= nearestEnemyIndices,
			DeltaTime 							= Time.deltaTime,
			ShotSpawnDataArchetype				= GameplayManager.GM.ShotSpawnDataArchetype,
			ParticleLineSpawnDataArchetype		= GameplayManager.GM.ParticleLineSpawnDataArchetype,
			CommandBuffer						= shotSpawnBarrier.CreateCommandBuffer(),
		}.Schedule(targetEnemyHandle);


		inputHandle = shootHandle;
#endregion Job Sceheduling
		

		return inputHandle;
	}

	protected override void OnStopRunning()
	{
		DisposePrevious();
	}

    private void DisposePrevious()
    {
		prevCells.NearestEnemyIndices.Dispose();
		prevCells.NearestEnemyDistance.Dispose();
    }


	[BurstCompile]
 	struct FindNearestEnemy : IJobParallelFor
    {
		public UnitData 								UnitData;
		public ShooterData								ShooterData;
		[ReadOnly] public NativeMultiHashMap<int, int> 	CellMap;
		public NativeArray<int> 						NearestEnemyIndices;
		public NativeArray<float> 						NearestEnemyDistance;

		const int maxIterations = 1000;
		
		public void Execute(int idx)
		{
			var currentPosition = UnitData.Positions[idx];
			var currentPlayerUnit = UnitData.PlayerUnits[idx];
			var entity = UnitData.Entities[idx];

			if (!ShooterData.ShooterEntities.Exists(entity)) return;

			var shooter = ShooterData.ShooterEntities[entity];

			int otherUnitIdx = 0;
			// int hash = GridHash.Hash(currentPosition.Value);
			NativeMultiHashMapIterator<int> iterator;
			
			int iterations = 0;
			
			int closestEnemyIdx = -1;
			float closestEnemyDist = 1000000f;
			
			// var quantizedPosition = currentPosition.Value.xy / GridHash.cellSize;
			// int2 flooredPosition = new int2(math.floor(quantizedPosition));

			int2 minXY = new int2((int)math.floor(currentPosition.Value.x - shooter.Range), (int)math.floor (currentPosition.Value.y - shooter.Range));
			int2 maxXY = new int2((int)math.floor (currentPosition.Value.x + shooter.Range), (int)math.floor (currentPosition.Value.y + shooter.Range));		

			int2 pos = minXY;
			int hash = GridHash.Hash(new float3(pos.x + 0.5f, pos.y + 0.5f, 0f));
			bool checkedAllGridPoints = false;
			bool found = CellMap.TryGetFirstValue(hash, out otherUnitIdx, out iterator);
			while (!checkedAllGridPoints)
			{
				if (found)
				{
					if (currentPlayerUnit.PlayerId != UnitData.PlayerUnits[otherUnitIdx].PlayerId)
					{
						FindClosestEnemy(idx, otherUnitIdx, ref closestEnemyIdx, ref closestEnemyDist);
					}
				}

				// Next iteration
				found = CellMap.TryGetNextValue(out otherUnitIdx, ref iterator);

				if (!found)
				{
					if (pos.x == maxXY.x && pos.y == maxXY.y)
					{
						checkedAllGridPoints =  true;
						continue;
					}
					if (pos.x < maxXY.x)
					{
						pos.x++;
					}else
					{
						pos.y++;
						pos.x = minXY.x;
					}

					float2 cellPos = (float2)pos / GridHash.cellSize;
					int2 flooredPosition = new int2(math.floor(cellPos));
					int nextHash = GridHash.Hash(flooredPosition);
					// Debug.Log("idx: "+ idx + ", pos: "+ pos + ", flooredPosition: "+ flooredPosition + ", thisHash: "+ GridHash.Hash(currentPosition.Value) + ", nextHash: "+ nextHash + ", iterations: " + iterations);

					found = CellMap.TryGetFirstValue(nextHash, out otherUnitIdx, out iterator);
				}

				if (++iterations > maxIterations) break;
			}

			NearestEnemyDistance[idx] = math.sqrt(closestEnemyDist);
			// Debug.Log("ClosestEnemyDist: "+ math.sqrt(closestEnemyDist) + ", minXY: "+ minXY + ", maxXY:  "+ maxXY + ", shooter.Range: "+ shooter.Range + ", position: "+ currentPosition.Value);
			NearestEnemyIndices[idx] = closestEnemyIdx;
		}

        private void FindClosestEnemy(int idx, int otherIdx, ref int closestEnemyIdx, ref float closestEnemyDist)
        {
			var thisPosition = UnitData.Positions[idx];
			var otherPosition = UnitData.Positions[otherIdx];

			float distance = math.lengthSquared(thisPosition.Value.xy - otherPosition.Value.xy);
			bool isNearest = distance < closestEnemyDist;

			closestEnemyDist = math.select(closestEnemyDist, distance, isNearest);
			closestEnemyIdx = math.select(closestEnemyIdx, otherIdx, isNearest); 
        }
    }

	struct TargetEnemy : IJobParallelFor
    {
		public UnitData 								UnitData;
		public ShooterData 								ShooterData;
		[ReadOnly] public NativeArray<int> 				NearestEnemyIndices;
		[ReadOnly] public NativeArray<float> 			NearestEnemyDistance;
		public float									RotationSpeed;
		public float									DeltaTime;
		public float									Time;

        public void Execute(int idx)
        {
			var entity = UnitData.Entities[idx];
			if (!ShooterData.ShooterEntities.Exists(entity)) return;

			if (ShooterData.ConstrucableEntities.Exists(entity))
			{
				var isComplete = Time > ShooterData.ConstrucableEntities[entity].CompleteTime;
           		if (!isComplete) return; //TODO Do ignore if has component instead?
			}


			bool isInEnemyDistance = NearestEnemyDistance[idx] < ShooterData.ShooterEntities[entity].Range;
			
			var shooterState = ShooterData.ShootingStateEntities[entity];
			var rotation = ShooterData.RotationEntities[entity];
			// var goalRotation = quaternion.identity;
			float3 heading = new float3();
			if (isInEnemyDistance)
			{
				int targetIdx = NearestEnemyIndices[idx];
				var targetEntity = UnitData.Entities[targetIdx];
				shooterState.TargetEntity = targetEntity;

				
				heading = math.normalize(UnitData.Positions[targetIdx].Value - UnitData.Positions[idx].Value);
				// rotation = new Rotation ( quaternion.euler(0, 0,  math.atan2(heading.y, heading.x)) );
				
				
				
			}else
			{
				shooterState.TargetEntity = new Entity();
				int playerId = UnitData.PlayerUnits[idx].PlayerId;
				heading = math.normalize(ShooterData.TargetEntities[entity].Position - UnitData.Positions[idx].Value);
				// goalRotation = playerId == 0 ? Quaternion.Euler(0, 0, 0) : Quaternion.Euler(0, 0, 180);
			}

			var goalRotation = quaternion.euler(0, 0,  math.atan2(heading.y, heading.x)) ;
			rotation.Value = quaternionx.RotateTowards(rotation.Value, goalRotation, RotationSpeed * DeltaTime);

			ShooterData.ShootingStateEntities[entity] = shooterState;
			ShooterData.RotationEntities[entity] = rotation;
        }
    }

    struct ShootAtEnemy : IJob
    {
		public UnitData 								UnitData;
		public ShooterData 								ShooterData;
		[ReadOnly] public NativeArray<int> NearestEnemyIndices;
		public float DeltaTime;
		public EntityArchetype ShotSpawnDataArchetype;
		public EntityArchetype ParticleLineSpawnDataArchetype;
		public EntityCommandBuffer CommandBuffer;

        public void Execute()
        {
			for (int idx = 0; idx < UnitData.Length; ++idx)
			{
				var entity = UnitData.Entities[idx];
				if (!ShooterData.ShooterEntities.Exists(entity)) return;
				// int unitIdx = ShooterToUnitMap[idx];
				var state = ShooterData.ShootingStateEntities[entity];
				// Debug.Log("Shot - idx: " + idx + ", state.Cooldown: "+ state.Cooldown + ", deltaTime: "+ DeltaTime);
				state.Cooldown -= DeltaTime;
				// if (state.Cooldown <= 0f && ShooterData.SpeedEntities[entity].speed <= 0f)
				if (state.Cooldown <= 0f && ShooterData.ShootingStateEntities[entity].TargetEntity != new Entity())
				{
					var shooting = ShooterData.ShooterEntities[entity];
					var pos = UnitData.Positions[idx];
					var targetIdx = NearestEnemyIndices[idx];

					state.Cooldown = shooting.ShootRate;

					if (shooting.WeaponId == GameplayManager.WEAPON_ID_LASER)
					{
						int damage = shooting.Damage;
						var targetHealth = UnitData.Healths[targetIdx];
						targetHealth.Value = math.max(targetHealth.Value - damage, 0);
						UnitData.Healths[targetIdx] = targetHealth;

						ParticleLineSpawnData spawn;
						spawn.StartPosition = pos;
						spawn.EndPosition = UnitData.Positions[targetIdx];
						spawn.ParticleLineId = 0; //TODO
						spawn.PlayerId = UnitData.PlayerUnits[idx].PlayerId;

						CommandBuffer.CreateEntity(ParticleLineSpawnDataArchetype);
						CommandBuffer.SetComponent(spawn);

					}else if (shooting.WeaponId == GameplayManager.WEAPON_ID_BULLET)
					{
						var targetPos = UnitData.Positions[targetIdx];
						float3 heading = math.normalize(targetPos.Value - pos.Value);

						ProjectileSpawnData spawn;
						spawn.WeaponId = shooting.WeaponId;
						spawn.Position = UnitData.Positions[idx];
						spawn.Rotation = new Rotation ( quaternion.euler(0, 0,  math.atan2(heading.y, heading.x)) );
						spawn.PlayerId = (UnitData.PlayerUnits[idx].PlayerId + 1) % 2;

						CommandBuffer.CreateEntity(ShotSpawnDataArchetype);
						CommandBuffer.SetComponent(spawn);
					}
				}

				ShooterData.ShootingStateEntities[entity] = state;
			}
        }
    }
}
