using System;
using Unity.Entities;

public class ShootingComponent : ComponentDataWrapper<Shooting>{}

[Serializable]
public struct Shooting : IComponentData
{
	public float Range;
    public float ShootRate;
	public int WeaponId;
	public int Damage;
}
