using Unity.Entities;
using Unity.Transforms;

public struct ProjectileSpawnData : IComponentData
{
    public int WeaponId;
    public Position Position;
    public Rotation Rotation;
    public int PlayerId;
}