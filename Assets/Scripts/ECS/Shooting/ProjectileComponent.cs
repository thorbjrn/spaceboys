using Unity.Entities;

public class ProjectileComponent : ComponentDataWrapper<Projectile>{}

public struct Projectile : IComponentData
{
    public int PlayerId;
}