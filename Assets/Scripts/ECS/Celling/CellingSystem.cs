﻿using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


public class CellingSystem : JobComponentSystem 
{
	public struct UnitData
	{
		public readonly int Length;
		[ReadOnly] public ComponentDataArray<Position> Positions;
		[ReadOnly] public ComponentDataArray<PlayerUnit> PlayerUnits;
		public ComponentDataArray<HealthState> HealthStates;
		[ReadOnly] public EntityArray Entities;
	}

	// private PrevCells prevCells = new PrevCells();
    public NativeMultiHashMap<int, int> CellMap;
	// private NativeList<Entity> entities;

    [Inject] private UnitData unitData;

	// protected override void OnCreateManager(int capacity)
	// {
	// 	entities = new NativeList<Entity>(Allocator.Persistent);
	// }

	protected override void OnDestroyManager()
	{
		// entities.Dispose();
		if (CellMap.IsCreated) CellMap.Dispose();
	}

    protected override JobHandle OnUpdate(JobHandle inputHandle)
    { 
        int unitCount = unitData.Length;

		// entities.ResizeUninitialized(unitCount);

		if (!CellMap.IsCreated) CellMap = new NativeMultiHashMap<int, int>(unitCount, Allocator.Persistent);
		else CellMap.Clear();
        CellMap.Capacity = math.max(CellMap.Capacity, unitCount);

		// var copyEntitiesHandle = new CopyEntities
		// {
		// 	EntitiesIn = unitData.Entities,
		// 	EntitiesOut = entities,

		// }.Schedule(unitCount, 64, inputHandle);

		var hashPositionsHandle = new HashPositions
		{
			Positions      = unitData.Positions,
			CellMap        = CellMap,
		}.Schedule(unitCount, 64, inputHandle);


        return hashPositionsHandle;
    }



	struct CopyEntities : IJobParallelFor
	{
		[ReadOnly]
		public EntityArray 								EntitiesIn;
		public NativeArray<Entity> 						EntitiesOut;

		public void Execute(int idx)
		{
			EntitiesOut[idx] = EntitiesIn[idx];
		}
	}


	// [BurstCompile]
	struct HashPositions : IJobParallelFor
	{
		[ReadOnly] public ComponentDataArray<Position> Positions;
		public NativeMultiHashMap<int, int>.Concurrent CellMap;

		public void Execute(int idx)
		{
			var hash = GridHash.Hash(Positions[idx].Value);
			CellMap.Add(hash, idx);
		}
	}

	// struct PrevCells
	// {
	// 	public NativeMultiHashMap<int, int> HashMap;
	// }
}


