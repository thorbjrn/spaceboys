﻿using System;
using Unity.Entities;
using Unity.Mathematics;

public class VelocityComponent : ComponentDataWrapper<Velocity>{}

[Serializable]
public struct Velocity : IComponentData
{
	public float3 Value;
}