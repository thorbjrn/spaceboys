using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


public class MovementSystem : JobComponentSystem 
{
    public struct Moveable
    {
        public readonly int Length;
        public ComponentDataArray<Position> Positions;
        [ReadOnly] public ComponentDataArray<Velocity> Velocities;
        [ReadOnly] public ComponentDataArray<Rotation> Rotations;
        [ReadOnly] public ComponentDataArray<MoveSpeed> MoveSpeeds;
        // [ReadOnly] public ComponentDataArray<Constructable> Constructables;
    }

    [Inject] public Moveable moveables;

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var moveJobHandle = new MovementJob
        {
            Moveables = moveables,
            DeltaTime = Time.deltaTime,
            Time = Time.time
        }.Schedule(moveables.Length, 64, inputDeps);

        return moveJobHandle;
    }


    [BurstCompile]
    struct MovementJob : IJobParallelFor
    {
        public Moveable Moveables;
        public float DeltaTime;
        public float Time;

        public void Execute(int idx)
        {
            // var isComplete = Time > Moveables.Constructables[idx].CompleteTime;
            // if (!isComplete) return; //TODO Do ignore if has component instead?

            var posComponent = Moveables.Positions[idx];

            var velocity = Moveables.Velocities[idx].Value;
            var rotation = Moveables.Rotations[idx].Value;
            var moveSpeed = Moveables.MoveSpeeds[idx].speed;
           
            

            var pos = posComponent.Value;
            
            // Debug.Log("idx: "+ idx + ", velocity: "+ velocity + ", deltaTime: "+ deltaTime + ", movespeed: "+ moveSpeed);
            pos += velocity * DeltaTime * moveSpeed;
            
            // var heading = math.mul(rotation, math.float3(1, 0, 0));
            // pos += deltaTime * moveSpeed * heading;

            posComponent.Value = pos;
            Moveables.Positions[idx] = posComponent;
        }
    }
}


