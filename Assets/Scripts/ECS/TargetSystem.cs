﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(CellingSystem))]
public class TargetSystem : JobComponentSystem {

    public struct MoveableData
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<Position> Positions;
        [ReadOnly] public ComponentDataArray<PlayerUnit> PlayerUnits;
        [ReadOnly] public ComponentDataArray<Rotation> Rotations;
        [ReadOnly] public ComponentDataArray<Constructable> Constructables;
        public ComponentDataArray<Velocity> Velocities;
        public ComponentDataArray<Target> Targets;
    }

    public struct ObjectivesData
    {
        public readonly int Length;
        [ReadOnly] public ComponentDataArray<Position> Positions;
        [ReadOnly] public ComponentDataArray<Objective> Objectives;
    }

    [Inject] private MoveableData moveables;
    [Inject] private ObjectivesData objectives;


    protected override JobHandle OnUpdate(JobHandle inputHandle)
    {
        var updateTargetHandle = new UpdateTargetPosition
        {
            Moveables   = moveables,
            Objectives  = objectives,
            DeltaTime   = Time.deltaTime,
            Time        = Time.time,
        }.Schedule(moveables.Length, 64, inputHandle);

        return updateTargetHandle;
    }


    // [BurstCompile]
    struct UpdateTargetPosition : IJobParallelFor
    {
        public MoveableData Moveables;
        public ObjectivesData Objectives;
        public float DeltaTime;
        public float Time;

        const float acceleration = 2f; //TODO
        const float damping = 8; //TODO
		const float maxVelocity = 1f; //TODO
		const float maxVelocitySqr = maxVelocity * maxVelocity;

        public void Execute(int idx)
        {
            var isComplete = Time > Moveables.Constructables[idx].CompleteTime;
            // if (!isComplete) return; //TODO Do ignore if has component instead?

            var velocity = Moveables.Velocities[idx];
            var target = Moveables.Targets[idx];

            int playerId = Moveables.PlayerUnits[idx].PlayerId;
            float2 pos = Moveables.Positions[idx].Value.xy;

            bool isInOpponentArea = (playerId == 0 && pos.x >  8.9f) || (playerId == 1 && pos.x < -8.9f); //TODO
            float2 targetPos = target.Position.xy;
            if (isInOpponentArea)
            {
                int closestObjectIdx = -1;
                float closestObjectiveDist = 100000f;
                for (int i = 0; i < Objectives.Length; i++)
                {
                    var objective = Objectives.Objectives[i];
                    if (objective.PlayerId != (playerId + 1) % 2) continue;
                    float2 objectivePos = Objectives.Positions[i].Value.xy;

                    float distance = math.lengthSquared(pos.xy - objectivePos.xy);
                    bool isNearest = distance < closestObjectiveDist;

                    closestObjectiveDist = math.select(closestObjectiveDist, distance, isNearest);
                    closestObjectIdx = math.select(closestObjectIdx, i, isNearest); 
                    targetPos = math.select(targetPos, objectivePos, isNearest);
                }
            }else
            {
                targetPos = pos + (playerId == 0 ? 1f : -1f) * new float2(1000, 0);
            }

            float2 newVel = velocity.Value.xy;
            float2 heading = math.normalize(targetPos - pos); 

            if (isComplete)
            {
                newVel += heading * acceleration * DeltaTime;
            }            

			float2 dampingStr = newVel * damping * DeltaTime;
			newVel -= dampingStr;

			if (math.dot(newVel, newVel) > maxVelocitySqr)
				newVel = math.normalize(newVel) * maxVelocity;

            velocity.Value = new float3(newVel.x, newVel.y, 0);
            
            Moveables.Velocities[idx] = velocity;
            Moveables.Targets[idx] = new Target{Position = new float3(targetPos.x, targetPos.y, 0)};
        }
    }
}
