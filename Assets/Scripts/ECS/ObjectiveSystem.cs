using Unity.Entities;

public class ObjectiveSystem : ComponentSystem
{
    struct ObjectivesData
    {
        public readonly int Length;

        public ComponentDataArray<Objective> Objectives;
    }

    [Inject] private ObjectivesData objectives;
    

    protected override void OnUpdate()
    {
        if (!GameplayManager.GM.IsGameActive) return;

        var aliveCount = new int[2];
        for (int i = 0; i < objectives.Length; i++)
        {
            var objective = objectives.Objectives[i];
            if (objective.PlayerId < 0 || objective.PlayerId > 1) continue;
            aliveCount[objective.PlayerId]++; 
        }

        int winnerId = -1;
        if (aliveCount[0] == 0) winnerId = 1;
        if (aliveCount[1] == 0) winnerId = 0;
        if (winnerId != -1)
        {
            GameplayManager.GM.PlayerWon(winnerId);
        } 
    }
}