using System;
using Unity.Entities;

public class LifeTimeComponent : ComponentDataWrapper<LifeTime>{}

[Serializable]
public struct LifeTime : IComponentData
{
	public float Time;

}
