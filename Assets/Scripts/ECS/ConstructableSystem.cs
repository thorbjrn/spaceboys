﻿// using System;
// using System.Collections;
// using System.Collections.Generic;
// using Unity.Burst;
// using Unity.Collections;
// using Unity.Entities;
// using Unity.Jobs;
// using Unity.Mathematics;
// using Unity.Transforms;
// using UnityEngine;

// public class ConstructableSystem : JobComponentSystem {

// 	public struct UnitData
// 	{
// 		public readonly int Length;
// 		[ReadOnly] public ComponentDataArray<Position> Positions;
// 		[ReadOnly] public ComponentDataArray<Constructable> Constructables;
// 		public ComponentDataArray<MoveSpeed> MoveSpeeds;
// 		public ComponentDataArray<ShootingState> ShootingStates;
// 	}

// 	[Inject] private UnitData unitData;


// 	protected override JobHandle OnUpdate(JobHandle inputHandle)
// 	{
// 		int unitCount = unitData.Length;

// #region Job Sceheduling

// 		var checkConstructionEndedHandle = new CheckConstructionEnded
// 		{
// 			UnitData				= unitData,
// 			Time					= Time.time,
// 		}.Schedule(unitCount, 64, inputHandle);

// 		inputHandle = checkConstructionEndedHandle;
// #endregion Job Sceheduling
	
// 		return inputHandle;
// 	}

// 	[BurstCompile]
//  	struct CheckConstructionEnded : IJobParallelFor
//     {
// 		public UnitData 								UnitData;
// 		public float 									Time;

// 		public void Execute(int idx)
// 		{
// 			var moveSpeed = UnitData.MoveSpeeds[idx];
// 			var shootingState = UnitData.ShootingStates[idx];

// 			float completeTime = UnitData.Constructables[idx].CompleteTime;
// 			bool isComplete = Time > UnitData.Constructables[idx].CompleteTime;
			
// 			moveSpeed.speed = math.select(0, )
// 		}
//     }

// }
