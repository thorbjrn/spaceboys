﻿using System;
using Unity.Entities;

public class ObjectiveComponent : ComponentDataWrapper<Objective>{}

[Serializable]
public struct Objective : IComponentData
{
	public int PlayerId;

}
