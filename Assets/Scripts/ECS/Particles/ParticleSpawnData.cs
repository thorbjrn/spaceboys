using Unity.Entities;
using Unity.Transforms;

public struct ParticleSpawnData : IComponentData
{
    public int ParticleId;
    public Position Position;
}