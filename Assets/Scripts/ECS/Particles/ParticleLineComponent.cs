using Unity.Entities;
using Unity.Transforms;

public class ParticleLineComponent : ComponentDataWrapper<ParticleLine>{}

public struct ParticleLine : IComponentData
{
    public Position StartPosition;
    public Position EndPosition;
    public int PlayerId;
    public float LifeTime;
}