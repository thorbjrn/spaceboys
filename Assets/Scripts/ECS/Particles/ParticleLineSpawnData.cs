using Unity.Entities;
using Unity.Transforms;

public struct ParticleLineSpawnData : IComponentData
{
    public int ParticleLineId;
    public Position StartPosition;
    public Position EndPosition;
    public int PlayerId;
}