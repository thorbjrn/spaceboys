using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;


public class ParticleSpawnBarrier : BarrierSystem {}

public class ParticleSpawnSystem : ComponentSystem {

    public struct ParticleSpawn
    {
        public readonly int Length;
        public EntityArray SpawnedEntities;
        [ReadOnly] public ComponentDataArray<ParticleSpawnData> Spawn;
    }

    public struct ParticleLineSpawn
    {
        public readonly int Length;
        public EntityArray SpawnedEntities;
        [ReadOnly] public ComponentDataArray<ParticleLineSpawnData> Spawn;
    }

    [Inject] private ParticleSpawn particleSpawns;
    [Inject] private ParticleLineSpawn particleLineSpawns;

    protected override void OnUpdate()
    {
        var entityManager = PostUpdateCommands;

        for (int i = 0; i < particleSpawns.Length; ++i)
        {
            var spawn = this.particleSpawns.Spawn[i];
            var particleEntity = this.particleSpawns.SpawnedEntities[i];

            ParticlesManager.PM.AddExplosionAt(spawn.Position.Value);
//            AudioManager.AM.PlayClip("pepSound2");
            
            PostUpdateCommands.DestroyEntity(particleEntity);

            // // Change spawn entity into shot entity, by removing and adding components
            // entityManager.RemoveComponent<ParticleSpawnData>(particleEntity);

            // entityManager.AddComponent(particleEntity, spawn.Position);
            // // entityManager.AddComponent(particleEntity, spawn.Rotation);
            // entityManager.AddComponent(particleEntity, default(TransformMatrix));
            // entityManager.AddComponent(particleEntity, new Particle { });
            // entityManager.AddSharedComponent(particleEntity, GameplayManager.GM.ProjectileRenderer);
        }

        for (int i = 0; i < particleLineSpawns.Length; ++i)
        {
            var spawn = this.particleLineSpawns.Spawn[i];
            var particleLineEntity = this.particleLineSpawns.SpawnedEntities[i];

            //Change spawn entity into shot entity, by removing and adding components
            entityManager.RemoveComponent<ParticleLineSpawnData>(particleLineEntity);

            // entityManager.AddComponent(particleLineEntity, spawn.StartPosition);
            // entityManager.AddComponent(particleLineEntity, default(TransformMatrix));
            entityManager.AddComponent(particleLineEntity, new LifeTime{ Time = 0.25f }); //TODO
            entityManager.AddComponent(particleLineEntity, new ParticleLine { StartPosition = spawn.StartPosition, EndPosition = spawn.EndPosition, LifeTime = 0.25f, PlayerId = spawn.PlayerId});
            entityManager.AddSharedComponent(particleLineEntity, GameplayManager.GM.LaserRenderer);
        }
    }
}

