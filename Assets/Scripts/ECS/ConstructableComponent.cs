﻿using System;
using Unity.Entities;

public class ConstructableComponent : ComponentDataWrapper<Constructable>{}

[Serializable]
public struct Constructable : IComponentData
{
	public float CompleteTime;
}
