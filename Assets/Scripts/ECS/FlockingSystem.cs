﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(TargetSystem))]
public class FlockingSystem : JobComponentSystem {

	public struct UnitData
	{
		public readonly int Length;
		[ReadOnly] public ComponentDataArray<Position> Positions;
		[ReadOnly] public ComponentDataArray<PlayerUnit> PlayerUnits;
		[ReadOnly] public ComponentDataArray<Flocking> Flockings;
		[ReadOnly] public EntityArray Entities;
	}

	[Inject] private ComponentDataFromEntity<Velocity> velocityEntities;

	[Inject] private UnitData unitData;
	[Inject] private CellingSystem cellingSystem;


	protected override JobHandle OnUpdate(JobHandle inputHandle)
	{
		int unitCount = unitData.Length;

#region Job Sceheduling

		var detectNeighboursHandle = new DetectNeighbours
		{
			UnitData					= unitData,
			VelocityEntities			= velocityEntities,
			CellMap						= cellingSystem.CellMap,
			DeltaTime					= Time.deltaTime,
		}.Schedule(unitCount, 64, inputHandle);

		inputHandle = detectNeighboursHandle;
#endregion Job Sceheduling
	
		return inputHandle;
	}

	[BurstCompile]
 	struct DetectNeighbours : IJobParallelFor
    {
		public UnitData 								UnitData;
		[NativeDisableParallelForRestriction]
		public ComponentDataFromEntity<Velocity> 		VelocityEntities;
		[ReadOnly] public NativeMultiHashMap<int, int> 	CellMap;
		public float 									DeltaTime;

		const float maxVelocity = 1f; //TODO
		const float maxVelocitySqr = maxVelocity * maxVelocity;
		const float epsilon = 0.00001f;
		const int maxIterations = 1000;
		
		public void Execute(int idx)
		{
			var entity = UnitData.Entities[idx];
			if (!VelocityEntities.Exists(entity)) return;
			
			var currentPosition = UnitData.Positions[idx];
			var currentPlayerUnit = UnitData.PlayerUnits[idx];
			var currentFlocking = UnitData.Flockings[idx];

			var currentVelocity = VelocityEntities[entity];

			float2 pos = currentPosition.Value.xy;
			float2 velocity = currentVelocity.Value.xy;

			int otherUnitIdx = 0;
			int hash = GridHash.Hash(currentPosition.Value);
			NativeMultiHashMapIterator<int> iterator;
			bool found = CellMap.TryGetFirstValue(hash, out otherUnitIdx, out iterator);
			int iterations = 0;
			int closestDone = 0;
			
			while (found)
			{
				if (otherUnitIdx == idx) // Exclude self
				{
					found = CellMap.TryGetNextValue(out otherUnitIdx, ref iterator);
					continue;
				}
				

				// if (currentPlayerUnit.PlayerId == UnitData.PlayerUnits[otherUnitIdx].PlayerId)
				// {
					// Construct the force direction
					var relative = pos - UnitData.Positions[otherUnitIdx].Value.xy;
					var otherRadius = UnitData.Flockings[otherUnitIdx].Radius;

					if (mathx.lengthSqr(relative) < epsilon || !math.any(relative)) relative = new float2(1, 0);

					float distanceSqr = mathx.lengthSqr(relative);

					float seperationDistSqr = (otherRadius + currentFlocking.Radius) / 2f;
					seperationDistSqr *= seperationDistSqr;
					if (seperationDistSqr < epsilon) seperationDistSqr = epsilon;

					// Update velocity data
					float str = math.max(0, seperationDistSqr - distanceSqr) / seperationDistSqr;
					velocity += math.normalize(relative) * str * currentFlocking.MaxForce * DeltaTime;
				// }

				// Next iteration
				found = CellMap.TryGetNextValue(out otherUnitIdx, ref iterator);

				// We will check 3 cells in the corner
				if (!found && closestDone < 3)
				{
					var position = currentPosition.Value.xy / GridHash.cellSize;
					var flooredPosition = new int2(math.floor(position));

					var nextQuantizedPosition = new int2(math.round(2f * position - (flooredPosition + new float2(0.5f))));
					if (nextQuantizedPosition.x == flooredPosition.x) nextQuantizedPosition.x -= 1;
					if (nextQuantizedPosition.y == flooredPosition.y) nextQuantizedPosition.y -= 1;

					if (closestDone == 1) nextQuantizedPosition.x = flooredPosition.x;
					else if (closestDone == 2) nextQuantizedPosition.y = flooredPosition.y;

					int nextHash = nextQuantizedPosition.x + GridHash.fieldWidthHalf + (nextQuantizedPosition.y + GridHash.fieldHeightHalf) * GridHash.fieldWidth;

					found = CellMap.TryGetFirstValue(nextHash, out otherUnitIdx, out iterator);

					closestDone++;
				}

				if (++iterations > maxIterations) break;
			}

			if (pos.y > 9) velocity.y -= (pos.y - 9f) * 10f * DeltaTime; //TODO
			if (pos.y < -9) velocity.y -= (pos.y + 9f) * 10f * DeltaTime; //TODO

			// var dampingStr = velocity * damping * DeltaTime;
			// velocity -= dampingStr;

			currentVelocity.Value = new float3(velocity.x, velocity.y, 0);
			if (math.dot(currentVelocity.Value, currentVelocity.Value) > maxVelocitySqr)
				currentVelocity.Value = math.normalize(currentVelocity.Value) * maxVelocity;

			VelocityEntities[entity] = currentVelocity;
		}
    }

}
