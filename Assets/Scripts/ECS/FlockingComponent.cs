﻿using System;
using Unity.Entities;

public class FlockingComponent : ComponentDataWrapper<Flocking>{}

[Serializable]
public struct Flocking : IComponentData
{
	// public float RadiusSqr;
	public float Radius;
	public float MaxForce;

    // public Flocking(float radius)
    // {
    //     RadiusSqr = radius * radius;
    // }
}
