﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class TargetComponent : ComponentDataWrapper<Target>{}

[Serializable]
public struct Target : IComponentData
{
	public float3 Position;
}
