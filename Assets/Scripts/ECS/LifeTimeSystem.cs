using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using UnityEngine;

public class RemoveExpired : BarrierSystem
{
}

/// <summary>
/// This system deletes entities that have a Health component with a value less than or equal to zero.
/// </summary>
public class LifeTimeSystem : JobComponentSystem
{
    struct Data
    {
        [ReadOnly] public EntityArray Entity;
        public ComponentDataArray<LifeTime> LifeTimes;
    }

    [Inject] private Data data;
    [Inject] private RemoveExpired removeExpired;

    [BurstCompile]
    struct RemoveReadJob : IJob
    {
        [ReadOnly] public EntityArray Entity;
        public ComponentDataArray<LifeTime> LifeTimes;
        public float DeltaTime;
        public EntityCommandBuffer Commands;

        public void Execute()
        {
            for (int i = 0; i < Entity.Length; ++i)
            {
                var lifeTime = LifeTimes[i];
                lifeTime.Time -= DeltaTime;
                if (lifeTime.Time <= 0.0f)
                {
                    Commands.DestroyEntity(Entity[i]);
                }
                LifeTimes[i] = lifeTime;
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        return new RemoveReadJob
        {
            Entity = data.Entity,
            LifeTimes = data.LifeTimes,
            DeltaTime = Time.deltaTime,
            Commands = removeExpired.CreateCommandBuffer(),
        }.Schedule(inputDeps);
    }
}


