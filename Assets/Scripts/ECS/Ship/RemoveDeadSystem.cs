using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Transforms;

public class RemoveDeadBarrier : BarrierSystem
{
}

/// <summary>
/// This system deletes entities that have a Health component with a value less than or equal to zero.
/// </summary>
[UpdateAfter(typeof(ShootingSystem))]
public class RemoveDeadSystem : JobComponentSystem
{
    struct Data
    {
        public readonly int Length;
        [ReadOnly] public EntityArray Entities;
        [ReadOnly] public ComponentDataArray<HealthState> Healths;
        [ReadOnly] public ComponentDataArray<Position> Positions;
    }

    [Inject] private Data data;
    [Inject] private RemoveDeadBarrier removeDeadBarrier;


    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        return new RemoveReadJob
        {
            Data = data,
            CommandBuffer = removeDeadBarrier.CreateCommandBuffer(),
            ParticleSpawnDataArchetype = GameplayManager.GM.ParticleSpawnDataArchetype,
        }.Schedule(inputDeps);
    }



    // [BurstCompile]
    struct RemoveReadJob : IJob
    {
        [ReadOnly] public Data Data;
        public EntityCommandBuffer CommandBuffer;
        public EntityArchetype ParticleSpawnDataArchetype;

        public void Execute()
        {
            for (int i = 0; i < Data.Length; ++i)
            {
                if (Data.Healths[i].Value <= 0.0f)
                {
                    ParticleSpawnData spawn;
                    spawn.Position = new Position{ Value = Data.Positions[i].Value };
                    spawn.ParticleId = 0; //TODO

                    CommandBuffer.CreateEntity(ParticleSpawnDataArchetype);
                    CommandBuffer.SetComponent(spawn);

                    CommandBuffer.DestroyEntity(Data.Entities[i]);
                }
            }
        }
    }

}


