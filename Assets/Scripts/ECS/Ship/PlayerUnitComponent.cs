using System;
using Unity.Entities;

public class PlayerUnitComponent : ComponentDataWrapper<PlayerUnit>{}

[Serializable]
public struct PlayerUnit : IComponentData
{
    public int PlayerId;
}


