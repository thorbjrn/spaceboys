using System;
using Unity.Entities;

public class HealthStateComponent : ComponentDataWrapper<HealthState>{}

[Serializable]
public struct HealthState : IComponentData
{
    public int Value;
}


