﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Jobs;

public class GameplayManager : MonoBehaviour
{

    public static GameplayManager GM;

    [SerializeField] public Player playerPrefab;
    [SerializeField] public GameObject fighterPrefab;
    [SerializeField] public GameObject artilleryPrefab;
    [SerializeField] int fighterCost = 5; //TODO
    [SerializeField] int artilleryCost = 50; //TODO
    [SerializeField] public int fighterBuildPerSecond = 500;
    [SerializeField] public int artilleryBuildPerSecond = 30;
    [SerializeField] public float topBound;
    [SerializeField] public float bottomBound;
    [SerializeField] public float leftBound;
    [SerializeField] public float rightBound;
    [SerializeField] public float buildRandomSpread = 1f;
    [SerializeField] private float shipConstructionTime = 1f;
    public float StartResourceIncome = 400;
    public float StartResourceAmount = 5000;

    public EntityArchetype ShotSpawnDataArchetype;
    public EntityArchetype ParticleSpawnDataArchetype;
    public EntityArchetype ParticleLineSpawnDataArchetype;
    public MeshInstanceRenderer ProjectileRenderer;


    public LineInstanceRenderer LaserRenderer;

    [SerializeField] private Faction[] factionSetup;

    private bool isGameActive;
    public bool IsGameActive { get { return isGameActive; } }
    private EntityManager entityManager;
    private Player[] players;
    private MeshInstanceRenderer[] fighterRenderers;
    private MeshInstanceRenderer[] artilleryRenderers;

    private List<Entity> backupObjectiveEntities = new List<Entity>();

    private float[] playerActionProgress = new float[2];

    public const int WEAPON_ID_LASER = 0;
    public const int WEAPON_ID_BULLET = 1;

    void Awake()
    {
        GM = this;

        entityManager = World.Active.GetOrCreateManager<EntityManager>();

        var prefabRenderer = fighterPrefab.GetComponent<MeshInstanceRendererComponent>().Value;
        fighterRenderers = new MeshInstanceRenderer[2];
        for (var i = 0; i < fighterRenderers.Length; ++i)
        {
            var mat = new Material(prefabRenderer.material);
            mat.color = factionSetup[i].color;
            fighterRenderers[i] = prefabRenderer;
            fighterRenderers[i].material = mat;
        }

        prefabRenderer = artilleryPrefab.GetComponent<MeshInstanceRendererComponent>().Value;
        artilleryRenderers = new MeshInstanceRenderer[2];
        for (var i = 0; i < artilleryRenderers.Length; ++i)
        {
            var mat = new Material(prefabRenderer.material);
            mat.color = factionSetup[i].color;
            artilleryRenderers[i] = prefabRenderer;
            artilleryRenderers[i].material = mat;
        }

        ShotSpawnDataArchetype = entityManager.CreateArchetype(typeof(ProjectileSpawnData));
        ParticleSpawnDataArchetype = entityManager.CreateArchetype(typeof(ParticleSpawnData));
        ParticleLineSpawnDataArchetype = entityManager.CreateArchetype(typeof(ParticleLineSpawnData));

        var entities = entityManager.GetAllEntities();
        foreach (var entity in entities)
        {
            if (entityManager.HasComponent<Objective>(entity))
            {
                var objective = entityManager.GetComponentData<Objective>(entity);
                var pos = entityManager.GetComponentData<Position>(entity);

                var backupEntity = entityManager.Instantiate(entity);
                entityManager.SetComponentData<Position>(backupEntity, new Position(new float3(pos.Value.x + 10000, pos.Value.y + 10000, pos.Value.z)));
                entityManager.SetComponentData<Objective>(backupEntity, new Objective{ PlayerId = objective.PlayerId + 10000 } );

                backupObjectiveEntities.Add(backupEntity);
                entityManager.DestroyEntity(entity);
            }
        }
        entities.Dispose();
    }


    public void StartGame(List<int> playerJoysticks)
    {
        ResetGame();

        players = new Player[2];
        for (int idx = 0; idx < 2; idx++)
        {
            var playerInstance = Instantiate(playerPrefab);
            playerInstance.Initialize(idx, factionSetup[idx], playerJoysticks[idx], PlayerAction);
            players[idx] = playerInstance;
        }

        foreach (var entity in backupObjectiveEntities)
        {
            var objective = entityManager.GetComponentData<Objective>(entity);
            var pos = entityManager.GetComponentData<Position>(entity);

            var newEntity = entityManager.Instantiate(entity);
            entityManager.SetComponentData<Position>(newEntity, new Position(new float3(pos.Value.x - 10000f, pos.Value.y - 10000f, -10)));
            entityManager.SetComponentData<Objective>(newEntity, new Objective{ PlayerId = objective.PlayerId - 10000 } );
        }
        isGameActive = true;
    }

    public void ResetGame()
    {
        if (players != null)
        {
            for (int i = 0; i < players.Length; i++) Destroy(players[i].gameObject);
            players = null;
        }

        var entities = entityManager.GetAllEntities();
        foreach (var entity in entities)
        {
            if (entityManager.HasComponent<Objective>(entity))
            {
                var objective = entityManager.GetComponentData<Objective>(entity);
                if (objective.PlayerId >= 1000) continue;
            }
            entityManager.DestroyEntity(entity);
        }
        entities.Dispose();
    }

    public void RestartGame()
    {
        var previousJoysticks = new List<int> { players[0].JoystickId, players[1].JoystickId };
        StartGame(previousJoysticks);
    }

    private void PlayerAction(int playerId, int shipId)
    {
        // Debug.Log("PlayerAction - id: " + playerId);
        if (!isGameActive) return;

        float buildSpeed = shipId == 0 ? fighterBuildPerSecond : artilleryBuildPerSecond;
        int cost = shipId == 0 ? fighterCost : artilleryCost;

        var player = players[playerId];
        playerActionProgress[playerId] += Time.deltaTime * buildSpeed;

        int maxAmount = (int)(playerActionProgress[playerId]);
        int amountCanAfford = (int)player.ResourceAmount / cost;
        int amount = math.min(maxAmount, amountCanAfford);

        player.ResourceAmount -= amount * cost;
        MenuManager.MM.UpdateResourceDial(playerId, player.ResourceAmount);

        if (amount > 0)
        {
            playerActionProgress[playerId] = 0;
            AddShips(amount, playerId, shipId);
        }


    }


    void AddShips(int amount, int playerId, int shipId)
    {
        var player = players[playerId];
        var pos = player.transform.position;
        var rotation = playerId == 0 ? Quaternion.Euler(0, 0, 0) : Quaternion.Euler(0, 0, 180);

        var tempEntity = entityManager.Instantiate(shipId == 0 ? fighterPrefab : artilleryPrefab);
        entityManager.SetSharedComponentData(tempEntity, shipId == 0 ? fighterRenderers[playerId] : artilleryRenderers[playerId]);

        var entities = new NativeArray<Entity>(amount, Allocator.Temp);
        entityManager.Instantiate(tempEntity, entities);
        entityManager.DestroyEntity(tempEntity);

        for (int i = 0; i < amount; i++)
        {
            float xVal = pos.x + UnityEngine.Random.Range(-buildRandomSpread, buildRandomSpread);
            float yVal = pos.y + UnityEngine.Random.Range(-buildRandomSpread, buildRandomSpread);
            entityManager.SetComponentData(entities[i], new Position { Value = new float3(xVal, yVal, -10) });
            entityManager.SetComponentData(entities[i], new Rotation { Value = rotation });
            entityManager.SetComponentData(entities[i], new Velocity { Value = new float3(playerId == 0 ? 1f : -1f, 0, 0) });
            entityManager.SetComponentData(entities[i], new PlayerUnit { PlayerId = playerId });
            entityManager.SetComponentData(entities[i], new ShootingState { TargetEntity = new Entity() });
            entityManager.SetComponentData(entities[i], new Constructable { CompleteTime = Time.time + shipConstructionTime }); 
        }
        entities.Dispose();

        // Debug.Log("Added ships - current entity count: " + entityManager.Debug.EntityCount);
    }


    public void PlayerWon(int playerId)
    {
        isGameActive = false;

        MenuManager.MM.ShowWinnerMenu(playerId);
    }

    void OnValidate()
    {
        var redArea = GameObject.Find("RedArea");
        var blueArea = GameObject.Find("BlueArea");
        if (redArea != null && blueArea != null && factionSetup.Length == 2)
        {
            var redFaction = factionSetup[0];
            var blueFaction = factionSetup[1];

            redArea.transform.localScale = new Vector3(redFaction.spawnArea.size.x, redFaction.spawnArea.size.y, 1f);
            redArea.transform.position = new Vector3(redFaction.spawnArea.center.x, redFaction.spawnArea.center.y, -1f);
            blueArea.transform.localScale = new Vector3(blueFaction.spawnArea.size.x, blueFaction.spawnArea.size.y, 1f);
            blueArea.transform.position = new Vector3(blueFaction.spawnArea.center.x, blueFaction.spawnArea.center.y, -1f);

            var redLineRend = redArea.GetComponentInChildren<MeshRenderer>();
            var blueLineRend = blueArea.GetComponentInChildren<MeshRenderer>();
            if (redLineRend != null && blueLineRend != null)
            {
                var redColor = factionSetup[0].color;
                redColor.a = 0.5f;
                var blueColor = factionSetup[1].color;
                blueColor.a = 0.5f;
                redLineRend.sharedMaterial.color = redColor;
                blueLineRend.sharedMaterial.color = blueColor;
            }
        }
    }

}
