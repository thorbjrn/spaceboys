using System.Linq;
using Unity.Transforms;
using UnityEngine;

public static class RectExtensions
{
    public static Vector3 FitPosition(this Rect rect, Vector3 pos)
    {
		if (pos.x > rect.xMax) pos.x = rect.xMax;
        else if (pos.x < rect.xMin) pos.x = rect.xMin;
        if (pos.y > rect.yMax) pos.y = rect.yMax;
        else if (pos.y < rect.yMin) pos.y = rect.yMin;
        return pos;
    }

}

public static class MeshExtensions
{
    public static string GetInfoText(this Mesh mesh)
    {
        var text = "Mesh: " + mesh.name + "\n" +
            "vertices:  " + string.Join(", ", mesh.vertices.Select(x => x.ToString()).ToArray()) + "\n" +
            "triangles: " + string.Join(", ", mesh.triangles.Select(x => x.ToString()).ToArray()) + "\n" +
            "uv:        " + string.Join(", ", mesh.uv.Select(x => x.ToString()).ToArray());
		return text;
    }

}

public static class ComponentExtensions
{
    
}
    