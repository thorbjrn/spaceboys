﻿using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

public struct GeneratePoints
{
    static public void RandomPointsInSphere(float3 center, float radius, ref NativeArray<float3> points)
    {
        var radiusSquared = radius * radius;
        var pointsFound = 0;
        var count = points.Length;
        while (pointsFound < count)
        {
            var p = new float3
            {
                x = Random.Range(-radius, radius),
                y = Random.Range(-radius, radius),
                z = Random.Range(-radius, radius)
            };
            if (math.lengthSquared(p) < radiusSquared)
            {
                points[pointsFound] = center + p;
                pointsFound++;
            }
        }
    }

    static public void RandomPointsOnCircle(float3 center, float radius, ref NativeArray<float3> points)
    {
        var count = points.Length;
        for (int i = 0; i < count; i++)
        {
            float angle = Random.Range(0.0f, Mathf.PI * 2.0f);
            points[i] = center + new float3
            {
                x = math.sin(angle) * radius,
                y = 0,
                z = math.cos(angle) * radius
            };
        }
    }
}

// math extended
public static partial class mathx
{
    public static float lengthSqr(float v) { return v * v; }

    public static float lengthSqr(float2 v) { return math.dot(v, v); }

    public static float lengthSqr(float3 v) { return math.dot(v, v); }

    public static float lengthSqr(float4 v) { return math.dot(v, v); }
}

public static partial class quaternionx
{
    
    public static quaternion RotateTowards(quaternion from, quaternion to, float maxDegreesDelta)
    {
        float angle = Quaternion.Angle(from, to); //TODO
        if (angle == 0.0f) return to;
        return math.slerp(from, to, math.min(1.0f, maxDegreesDelta / angle));
    }   
}