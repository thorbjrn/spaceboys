﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    
    public static AudioManager AM;

    [SerializeField] private int maxAmountSources;
    [SerializeField] private AudioClip[] clips;
    
    private Queue<AudioSource> sources = new Queue<AudioSource>();
//    private Queue<AudioSource> activeSources = new Queue<AudioSource>();
    private int sourceCount;
    
    void Awake() {
        AM = this;
    }

    private AudioSource GetFreeSource() {
        AudioSource result = null;
        if (sources.Count > 0) result = sources.Dequeue();
        if (result == null && sourceCount < maxAmountSources) {
            result = new GameObject("AudioSource").AddComponent<AudioSource>();
            sourceCount++;
        } 

        return result;
    }
    
    private AudioClip GetClip(string name) {
        return clips.FirstOrDefault(x => x.name == name);
    }

    public void PlayClip(string name) {
        var source = GetFreeSource();
        var clip = GetClip(name);
        if (source == null) { 
            Debug.LogError("No AudioSource available!");
            return;
        }
        if (clip == null) { 
            Debug.LogError("No clip with name " + name + " found!");
            return;
        }

        source.clip = clip;
        source.Play();


        StartCoroutine(ReturnSource(source));
    }

    private IEnumerator ReturnSource(AudioSource source) {
        yield return new WaitForSeconds(source.clip.length);
        
        source.Stop();
        sources.Enqueue(source);
    }
}