using System;
using UnityEngine;

[Serializable]
public class Faction
{
    public Color color;
    public Rect spawnArea;
}