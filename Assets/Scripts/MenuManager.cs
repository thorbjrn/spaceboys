﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour 
{
	public static MenuManager MM;

	[SerializeField] private RectTransform mainMenu;
	[SerializeField] private RectTransform settingsMenu;
	[SerializeField] private RectTransform winnerMenu;

	[SerializeField] private Canvas menuUI;
	[SerializeField] private Canvas playerSelectUI;
	[SerializeField] private Canvas gameUI;

	[SerializeField] private Text[] resourceTexts;
	[SerializeField] private Text winnerText;

    void Awake()
	{
		MM = this;
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter) && !GameplayManager.GM.IsGameActive) ForceStartGame();
    }

    [ContextMenu("Force Start Game")]
    public void ForceStartGame()
    {
        PlayersJoined(new List<int> { -1, -1 });
    }

	public void ClickedStart()
	{
		Debug.Log("ClickedStart");

		menuUI.gameObject.SetActive(false);
		playerSelectUI.gameObject.SetActive(true);
	}

	public void ClickedSettings()
	{
		Debug.Log("ClickedSettings");

		mainMenu.gameObject.SetActive(false);
		settingsMenu.gameObject.SetActive(true);
	}

	public void ClickedExit()
	{
		Debug.Log("ClickedExit");

		Application.Quit();
	}

	public void ClickedOpenMainMenu()
	{
		Debug.Log("ClickedOpenMainMenu");

		GameplayManager.GM.ResetGame();
		winnerMenu.gameObject.SetActive(false);
		settingsMenu.gameObject.SetActive(false);
		mainMenu.gameObject.SetActive(true);
		gameUI.gameObject.SetActive(false);
		menuUI.gameObject.SetActive(true);

		mainMenu.GetComponentInChildren<Button>().Select();
	}

	public void ClickedRematch()
	{
		playerSelectUI.gameObject.SetActive(false); 
		winnerMenu.gameObject.SetActive(false);
		GameplayManager.GM.RestartGame();
	}
	

    public void UpdateResourceDial(int playerId, float resourceAmount)
    {
		resourceTexts[playerId].text = ""+(int)resourceAmount; 
    }

    internal void PlayersJoined(List<int> playerJoysticks)
    {
		playerSelectUI.gameObject.SetActive(false); 
		gameUI.gameObject.SetActive(true);
		menuUI.gameObject.SetActive(false);

		GameplayManager.GM.StartGame(playerJoysticks);
    }

    public void ShowWinnerMenu(int playerId)
    {
		winnerMenu.gameObject.SetActive(true);
        winnerText.text = (playerId == 0 ? "Red" : "Blue") + " player won!";

		winnerMenu.GetComponentInChildren<Button>().Select();
    }

	
}
