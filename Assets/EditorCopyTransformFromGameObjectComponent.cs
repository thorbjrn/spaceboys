﻿using Unity.Entities;

namespace Unity.Transforms
{
    /// <summary>
    /// Copy Transform from GameObject associated with Entity to TransformMatrix.
    /// </summary>
    public struct EditorCopyTransformFromGameObject : IComponentData { }

    public class EditorCopyTransformFromGameObjectComponent : ComponentDataWrapper<EditorCopyTransformFromGameObject> { } 
}
