﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boot : MonoBehaviour {

	[SerializeField] private bool debugGame;


	void Start()
	{
		if (debugGame)
		{
			MenuManager.MM.ForceStartGame();
		}else
		{
			MenuManager.MM.ClickedOpenMainMenu();
		}
	}
}
